

public class KalahMiniMax extends KalahAgent {
	
	private int maxDepth;
	private boolean player;
	
	public KalahMiniMax(int maxDepth, boolean player) {
		if(maxDepth < 1) System.out.println("depth must be greater than 1");
		this.maxDepth = maxDepth;
		this.player = player;
	}
	
	@Override 
	public int nextMove(KalahBoard board) {		
			float max = Float.NEGATIVE_INFINITY;
			int move = 0;
			
			//System.out.println("moving...");
			//System.out.println("player=" + player);
			
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, player);
					
					float n = miniMax(child, maxDepth - 1);
					
					if(n > max) {
						max = n;
						move = i;
					}
					//System.out.println("[" + i + ":" + n + "]");
				}
			}
			
			//System.out.println("max=" + max + " move=" + move);
			//System.out.println("------------------");
			return move;
	}

	public float miniMax(KalahBoard board, int depth) {
		if(depth == 0 || board.isGameOver())
			return score(board, player);
		
		if(board.getTurn() == player) {
			//Maximize
			float best = -2000f;
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, player);
					float n = miniMax(child, depth - 1);
					if(n > best) best = n;
				}
			}
			return best;
		}
		else {
			//Minimize
			float best = 2000f;
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, !player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, !player);
					float n = miniMax(child, depth - 1);
					if(n < best) best = n;
				}
			}
			return best;
		}
	}
	
	
	public static float score(KalahBoard board, boolean player) {
		if(board.isGameOver()) {
			if(board.getPlayerScore(player) > board.getPlayerScore(!player))
				return 1000f;
			else return -1000f;
		}
		return board.getPlayerScore(player) - board.getPlayerScore(!player);
	}
}
