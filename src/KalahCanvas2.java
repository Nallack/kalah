import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.*;

public class KalahCanvas2 extends JPanel {

	private KalahGame game;
	private JButton[] botButtons;
	private JButton[] topButtons;

	
	
	public KalahCanvas2(KalahGame game) {
		this.game = game;
		
		botButtons = new JButton[6];
		topButtons = new JButton[6];
		for(int i = 0; i < 6; i++) {
			
		}
		
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		draw((Graphics2D)g);
	}
	
	protected void draw(Graphics2D graphics) {
		drawGuides(graphics);
		drawTiles(graphics);
	}
	
	private void drawTiles(Graphics2D graphics) {
		Dimension size = getSize();
		
		int twidth = size.width * 2 / 21;
		int theight = size.height / 6;
		
		int xpad = size.width / 21;
		int ypad = size.height / 6;
		
		int xjump = size.width / 21;
		int mtop = size.height / 8;
		int mbot = size.height * 7 / 8 - theight;
		int mmid = size.height / 2 - theight / 2;
		
		for(int i = 0; i < 6; i++) {
			graphics.setColor(Color.WHITE);
			
			graphics.fillRect(
					2 * xpad + (twidth + xjump) * i,
					mtop,
					twidth,
					theight);
			graphics.fillRect(
					2 * xpad + (twidth + xjump) * i,
					mbot,
					twidth,
					theight);
			
			graphics.setColor(Color.BLACK);
			graphics.setFont(new Font("Arial", 0, 20));
			graphics.drawString(
					Integer.toString(game.board.getTop(5 - i)),
					2 * xpad + (twidth + xjump) * i + twidth/2,
					mtop + theight/2);
			graphics.drawString(
					Integer.toString(game.board.getBot(i)),
					2 * xpad + (twidth + xjump) * i + twidth/2,
					mbot + theight/2);
		}
		
		
		//mid
		graphics.setColor(Color.WHITE);
		graphics.fillRect(
				xpad,
				mmid,
				twidth,
				theight);
		graphics.setColor(Color.BLACK);
		graphics.drawString(
				Integer.toString(game.board.getTopScore()),
				xpad/2 + twidth/2,
				mmid + theight/2);
		
		graphics.setColor(Color.WHITE);
		graphics.fillRect(
				size.width - xpad - twidth,
				mmid,
				twidth,
				theight);
		graphics.setColor(Color.BLACK);
		graphics.drawString(
				Integer.toString(game.board.getBotScore()),
				size.width - xpad/2 - twidth/2,
				mmid + theight/2);
	}
	
	private void drawGuides(Graphics2D graphics) {
		Dimension size = getSize();
		int gheight = size.height / 3;
		
		graphics.setColor(Color.BLUE);
		if(!game.board.getTurn()) {
		graphics.fillRect(
				0,
				0,
				size.width,
				gheight);
		} else {
			graphics.fillRect(
				0,
				size.height - gheight,
				size.width,
				gheight);
		}
	}
}
