import java.util.Random;

public class KalahRandom extends KalahAgent {
	
	Random r;
	boolean player;
	
	public KalahRandom(boolean player) {
		this.r = new Random();
		this.player = player;
	}
	
	@Override
	public int nextMove(KalahBoard board) {	
		int x = 0;
		do {
			x = r.nextInt(6);
		} while(board.canPlayerMove(x, player));
		return x;
	}

}
