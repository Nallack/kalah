import java.util.Arrays;


public class KalahBoard {
	
	//size = 6
	//  12 11 10 09 08 07
	// 13               06
	//  00 01 02 03 04 05
	
	public int[] values;
	private int size;
	
	//true if bottom player's turn
	private boolean turn = true;
	
	public KalahBoard(int size) {
		this.size = size;
		this.values = new int[2 * size + 2];
	}
	
	public KalahBoard(int size, int start) {
		this.size = size;
		this.values = new int[2 * size + 2];
		
		Arrays.fill(values, start);
		values[size] = 0;
		values[2 * size + 1] = 0;
	}

	//region getters/setters
	public int getSize() { return size; }
	public boolean getTurn() { return turn; }
	public int getPlayerScore(boolean player) {
		if(player) return values[size];
		else return values[size * 2 + 1];
	}
	public int getBotScore() { return values[size]; }
	public int getTopScore() { return values[size * 2 + 1]; }
	
	public int getPlayer(int x, boolean player) {
		if(player) return values[x];
		else return values[size + x + 1];
	}
	public int getBot(int x) { return values[x]; }
	public int getTop(int x) { return values[size + 1 + x]; }
	//endregion
	
	public void movePlayer(int x, boolean player) {
				
		if(turn != player)
			throw new IllegalArgumentException("not player's turn");
		if(!canPlayerMove(x, player))
			throw new IllegalArgumentException("illegal move");
		
		int offset = 0;
		if(!player) offset = size + 1;
		x += offset;
		int n = values[x];
		values[x] = 0;
		while(n-- > 0) {
			x = (x + 1) % values.length;
			values[x]++;
		}
		
		if(player) {
			if(0 <= x && x < size && values[x] == 1) {
				int y = size * 2 - x;
				values[size] += values[x] + values[y];
				values[x] = 0;
				values[y] = 0;
			}
		} else {
			if(size < x && x <= size * 2 && values[x] == 1) {
				int y = size * 2 - x;
				values[size * 2 + 1] += values[x] + values[y];
				values[x] = 0;
				values[y] = 0;
			}
		}
				
		if((x == size + offset && canPlayerMove(player)) || !canPlayerMove(!player)) turn = player;
		else turn = !player;
	}
	
	public boolean canPlayerMove(boolean player) {
		boolean canMove = false;
		for(int i = 0; i < size; i++) {
			if(getPlayer(i, player) != 0) canMove = true;
		}
		return canMove;
	}
	
	public boolean canPlayerMove(int x, boolean player) {
		if(player) return values[x] != 0;
		else return values[size + x + 1] != 0;
	}
	
	public boolean canBotMove(int x) {
		return getBot(x) != 0;
	}
	
	public boolean canTopMove(int x) {
		return getTop(x) != 0;
	}
	
	public boolean isGameOver() {
		boolean gameOver = true;
		for(int i = 0; i < size; i++) {
			if(values[i] != 0) gameOver = false;
			if(values[i + size + 1] != 0) gameOver = false;
		}
		return gameOver;
	}
	
	public String toString() {
		String s = "";
		for(int n : values) s += "["+ n + "]";
		return s;
	}
	
	public KalahBoard clone() {
		KalahBoard board = new KalahBoard(size);
		board.values = values.clone();
		board.turn = turn;
		return board;
	}
}
