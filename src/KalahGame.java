
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class KalahGame extends JFrame {
	private static final long serialVersionUID = -5504534540775796558L;
	
	protected JPanel controlsPanel;
	
	protected String[] players = {"human", "minimax", "alphabeta"};
	protected Integer[] levels = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20};
	
	protected JPanel topPanel;
	protected JLabel topLabel;
	protected JComboBox<String> topType;
	protected JComboBox<Integer> topLevel;
	
	protected JPanel botPanel;
	protected JLabel botLabel;
	protected JComboBox<String> botType;
	protected JComboBox<Integer> botLevel;
	
	protected JPanel gamePanel;
	protected JButton startButton;
	protected JButton resetButton;
	
	protected JPanel canvas;
	protected KalahBoard board;
	protected KalahAgent topAgent = null;
	protected KalahAgent botAgent = null;
	protected KeyAdapter playerMove;
	
	public KalahGame() {
		super("KalahGame");
		
		board = new KalahBoard(6, 6);
		
		initUI();
		canvas.requestFocus();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void initUI() {
		this.setMinimumSize(new Dimension(600, 400));
		
		controlsPanel = new JPanel(new BorderLayout());
		{
			topPanel = new JPanel();
			topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
			{
				topLabel = new JLabel("Player 2");
				topPanel.add(topLabel);
				
				//default
				topType = new JComboBox<String>(players);
				topType.addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent arg0) {
						setTopPlayer(topType.getSelectedIndex(), (Integer)topLevel.getSelectedItem());
					}
				});
				topPanel.add(topType);
				
				
				topLevel = new JComboBox<Integer>(levels);
				topLevel.addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent arg0) {
						setTopPlayer(topType.getSelectedIndex(), (Integer)topLevel.getSelectedItem());
					}
				});
				topPanel.add(topLevel);
			}
			controlsPanel.add(topPanel, BorderLayout.NORTH);
			
			gamePanel = new JPanel(new GridBagLayout());
			{
				GridBagConstraints constraints = new GridBagConstraints();
				constraints.fill = GridBagConstraints.HORIZONTAL;
				constraints.gridy = 0;
				startButton = new JButton("Start");
				startButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ready();
					}
				});
				gamePanel.add(startButton, constraints);
				
				constraints.gridy = 1;
				resetButton = new JButton("Reset");
				resetButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						board = new KalahBoard(6, 6);
						repaint();
					}
				});
				gamePanel.add(resetButton, constraints);
			}
			controlsPanel.add(gamePanel, BorderLayout.CENTER);
			
			botPanel = new JPanel();
			botPanel.setLayout(new BoxLayout(botPanel, BoxLayout.Y_AXIS));
			{
				botLabel = new JLabel("Player 1");
				botPanel.add(botLabel);
				
				botType = new JComboBox<String>(players);
				botType.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setBotPlayer(botType.getSelectedIndex(), (Integer)botLevel.getSelectedItem());
					}
				});
				botPanel.add(botType);
				
				
				botLevel = new JComboBox<Integer>(levels);
				botLevel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setBotPlayer(botType.getSelectedIndex(), (Integer)botLevel.getSelectedItem());
					}
				});
				botPanel.add(botLevel);
			}
			controlsPanel.add(botPanel, BorderLayout.SOUTH);
			
		}
		this.add(controlsPanel, BorderLayout.EAST);
		
		canvas = new KalahCanvas(this);
		canvas.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				try {
					int x = Integer.parseInt(Character.toString(e.getKeyChar()));
					if(1 <= x && x <= 6) {
						boolean player = board.getTurn();
						if(player) move(x - 1, player);
						else move(6 - x, player);
					}
				} catch (NumberFormatException ex) { }
			}
		});
		this.add(canvas, BorderLayout.CENTER);
	}
	
	private void setTopPlayer(int agent, int level) {
		switch(agent) {
		case 0:
			topAgent = null;
			break;
		case 1:
			topAgent = new KalahMiniMax(level, false);
			break;
		case 2:
			topAgent = new KalahAlphaBeta(level, false);
			break;
		}
	}
	
	
	private void setBotPlayer(int agent, int level) {
		switch(agent) {
		case 0:
			botAgent = null;
			break;
		case 1:
			botAgent = new KalahMiniMax(level, true);
			break;
		case 2:
			botAgent = new KalahAlphaBeta(level, true);
			break;
		}
	}
	
	private void move(int x, boolean player) {
		if(board.canPlayerMove(x, player)) {
			board.movePlayer(x, player);
			canvas.paintImmediately(canvas.getVisibleRect());
			ready();
		}
		else System.out.println("invalid move...");
	}
	
	private void ready() {
		//check if an agent is due to make a move		
		if(!board.isGameOver()) {
			if(board.getTurn()) {
				if(botAgent != null) {
					runBotAI();
					ready();
				}
			} else {
				if(topAgent != null) {
					runTopAI();
					ready();
				}
			}
		}
	}
	
	private void runBotAI() {	
		
		try { Thread.sleep(10); } catch(Exception ex) { }
			
		//debug, ai shouldnt make invalid moves
		int move = botAgent.nextMove(board);
		if(board.canBotMove(move)) {
			board.movePlayer(move, true);
		}
		else {
			System.out.println("bot ai made invalid move : " + move);
		}
		
		canvas.paintImmediately(canvas.getVisibleRect());
	}
	
	private void runTopAI() {
										
		try { Thread.sleep(10); } catch(Exception ex) { }
			
		//debug, ai shouldnt make invalid moves
		int move = topAgent.nextMove(board);
		if(board.canTopMove(move)) {
			board.movePlayer(move, false);
		} else {
			System.out.println("top ai made invalid move : " + move);
		}
		
		canvas.paintImmediately(canvas.getVisibleRect());
	}
}