

public class KalahAlphaBeta extends KalahAgent {
	
	private int maxDepth;
	private boolean player;
	
	public KalahAlphaBeta(int maxDepth, boolean player) {
		this.maxDepth = maxDepth;
		this.player = player;
	}
	
	@Override 
	public int nextMove(KalahBoard board) {		
			float max = Float.NEGATIVE_INFINITY;
			int move = 0;
			
			
			//not sure if this works properly for multiple turns in a row
			//cant prune if previous turn == current turn
			
			//pass a boolean of the who's turn it was before the board state
			
			
			//System.out.println("moving...");
			//System.out.println("player=" + player);
			
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, player);	
					float n = alphaBeta(child, maxDepth - 1, -2000f, 2000f, board.getTurn() != child.getTurn());
					if(n > max) {
						max = n;
						move = i;
					}
					//System.out.println("[" + i + ":" + n + "]");
				}
			}
			
			//System.out.println("max=" + max + " move=" + move);
			//System.out.println("------------------");
			return move;
	}

	public float alphaBeta(KalahBoard board, int depth, float alpha, float beta, boolean canPrune) {
		if(depth == 0 || board.isGameOver())
			return score(board, player);
		
		if(board.getTurn() == player) {
			//Maximize
			float best = -2000f;
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, player);
					float n = alphaBeta(child, depth - 1, alpha, beta, board.getTurn() != child.getTurn());
					if(n > best) best = n;
					if(best > alpha) alpha = best; 
					if(canPrune && beta <= alpha) break;
				}
			}
			return best;
		}
		else {
			//Minimize
			float best = 2000f;
			for(int i = 0; i < board.getSize(); i++) {
				if(board.canPlayerMove(i, !player)) {
					KalahBoard child = board.clone();
					child.movePlayer(i, !player);
					float n = alphaBeta(child, depth - 1, alpha, beta, board.getTurn() != child.getTurn());
					if(n < best) best = n;
					if(best < beta) beta = best; 
					if(canPrune && beta <= alpha) break;
				}
			}
			return best;
		}
	}
	
	
	public static float score(KalahBoard board, boolean player) {
		float score = 0f;
		if(board.isGameOver()) {
			if(board.getPlayerScore(player) > board.getPlayerScore(!player)) score += 1000f;
			else score -= 1000f;
		}
		score += board.getPlayerScore(player) - board.getPlayerScore(!player);
		return score;
	}
}
