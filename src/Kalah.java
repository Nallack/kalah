
import javax.swing.*;

public final class Kalah {
	
	public static void main(String[] args) {
		
		//use system default look/feel
		
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new KalahGame().setVisible(true);
			}
		});
	}
}