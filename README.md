# The Game of Kalah #

Kalah is a game where players take turns moving counters to try and increase their score on the right side of the board.

This version is an AI demonstration of minimax, with 1, 2, or no human players.